# Техническое задание
Техническое задание на создание программного обеспечения Карта кампуса СФУ «U-Maps» в соответствии [ГОСТ 34.602-89](http://docs.cntd.ru/document/gost-34-602-89)

В процессе разработки некоторые пункты данного документа могут быть изменены.
После загрузки приложения в открытый доступ необходимо создание небольшой группы операторов, следящих за работоспособностью приложения и создающих актуальные обновления данных.
В дальнейшем возможно расширение функционала данного приложения.
Сроки могут уточняться после выполнения любого из этапов работы.

## 1. Общие сведения о системе

### 1.1 Полное наименование системы и ее условное обозначение
Электронная карта кампуса Сибирского Федерального Университета «U-Maps».

### 1.2 Наименование предприятий (объединений) разработчика и заказчика (пользователя) системы
Разработчик — Проектная команда U-Maps, заказчик — СФУ.
	
### 1.3 Плановые сроки начала и окончания работы по созданию системы
03.09.2018 — 30.11.2018.

### 1.4 Назначение и область применения	
Мобильное приложение предназначено для упрощения процесса сбора, поиска и получения информации студентами, их родителями, гостями университета. Разрабатываемое нами приложение сокращает время поиска нужного помещения. Создание электронной карты территории кампуса СФУ и реализации в виде Android-приложения с описанием каждого кабинета и его назначения для организации функции поиска. Это поможет найти нужный кабинет, кафедру, отдел.

### 1.5 Перечень документов, на основании которых создается система
Разработанный исходный код программы должен быть под лицензией «General Public License», и выложен на веб-сервисе контроля версий GitLab. Программа должна соответствовать определениям открытого ПО от Open Source Initiative, а так же представлена в веб-сервисе цифровой дистрибуции Google Play.

### 1.6 Порядок предоставления
Презентация приложения, установка приложения на смартфон с Google Play, демонстрация работы приложения заказчику.

## 2. Требования к приложению:
### 2.1 Функциональные требования к приложению:
- Возможность выбора языка меню (для иностранных пользователей)
- Возможность просмотра карты кампуса СФУ
- Возможность просмотра плана этажа корпусов в режиме просмотра корпуса
- Возможность поиска аудитории/помещения/объекта по БД и его местоположения на карте
- Возможность прокладывать маршрут из точки А в точку Б (точки — объекты на арте)
- Возможность использования служб геолокации
- Возможность получать данные о кабинетах/аудиториях/помещениях/корпусах

### 2.2 Требования к безопасности
Надежное и устойчивое функционирование приложения (системы) должно быть обеспечено разработчиками посредством исключения возможности допуска пользователя к исходному коду и картам приложения. Помимо этого, уязвимость	приложения через так называемые «Лаги», а так же уязвимость вирусным ПО должны быть сведены к нулю. В противном	случае приложение может быть не в состоянии выполнять возможенные на него функции и придёт в негодность. После решения данных задач требуется сделать приложение менее ресурсоёмким, чтобы оно в последствии не требовало стабильно высокой скорости интернет-соединения и не тратило трафик пользователей без их ведома.

## 3. Человеческий ресурс
### Минимальное количество людей, требуемых для создания данного приложения (предлагается к обсуждению!):
- 2 android — разработчика;
- 1 дизайнер;
- 1 проект-менеджер;
- 1 backend — разработчик;
- 2 лингвиста (для перевода текста меню и подсказок)
- 2 картографа (в т.ч. для отрисовки трехмерной (либо псевдотрёхмерной) модели карт кампуса.

Функциональные обязанности персонала также предлагаются к обсуждению после утверждения минимального количества человек, требуемых для создания данного приложения.

## 4. Стадии и этапы разработки.
### 4.1 Стадии разработки:
- Разработка технического задания
- Разработка плана создания приложения
- Описание документации
- Разработка приложения
- Тестирование приложения
- Загрузка приложений в общий доступ

### 4.2 Содержание работ по этапу «Разработка приложения»:

#### 1. Проектирование приложения
- 1.1 Проработка структуры
- 1.2 Подбор цветовых решений для интерфейса
- 1.3 Первичное согласование проекта

#### 2. Решение вопроса об используемом инструментарии
- 2.1 Инструменты для описания интерфейса приложения (меню)
- 2.2 Инструменты для создания карт кампуса
- 2.3 Инструменты для интеграции карт в приложение
- 2.4 Инструменты для серверной части (загрузка языковых схем и обновлений на картах)
- 2.5 Вторичное согласование проекта

#### 3. Написание кода приложения
##### 3.1 Создание интерфейса (всех возможных экранов меню)
- Экран загрузки
- Меню выбора языка
- Основное меню
- Строка поиска
- Функциональные кнопки
- Меню списка карт по площадкам
- Экран настроек
- Экран обратной связи
- Экран информации о разработчиках

##### 3.2 Создание трёхмерных карт
- Получение планов всех пощадок
- Получение планов всех помещений
- Получение планов всех этажей
- Разработка значков объектов
- Проектирование карт

##### 3.3 Интеграция карт в приложение
- Реализация интеграции карты в приложение
- Реализация поиска по карте
- Реализация навигации
- Реализация проложения маршрута

##### 3.4 Возможность получения подсказок об объектах на карте по щелчку
- Создание активных областей с информацией
- Заполнение блоков информации о каждом объекте

##### 3.5 Взаимодействие с сервером
- Реализация загрузки языковой схемы
- Реализация загрузки информации об объектах на карте
- Реализация загрузки обновлений на карте

##### 3.6 Создание сервера приложения
- Реализация связи с устройством пользователя
- Реализация обмена данными
- Формат данных объекта
- Формат данных языковой схемы
- Формат данных карт

#### 4. Тестирование приложения
- 4.1. Альфа-тестирование (разработчиками);
- 4.2. Поиск контрольной группы;
- 4.3. Бета-тестирование;
- 4.4. Баг репорт;
- 4.5. Доработка приложения и повторное тестирование;
- 4.6. Загрузка приложения в открытый доступ.